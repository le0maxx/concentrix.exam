﻿using concentrix.exam.Infrastructure;
using concentrix.exam.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace concentrix.exam.Controllers
{
    [Route("api/[controller]/")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly MyDBContext _myDBContext;
        public CustomerController(MyDBContext myDBContext)
        {
            _myDBContext = myDBContext;
        }

        [HttpGet]
        public IList<CustomerModel> Get() => _myDBContext.CustomerModel.ToList();

        [HttpPost("AddCustomer")]
        public ResponseModel AddCustomer([FromBody] CustomerRequestModel cus)
        {
            var res = new ResponseModel();
            try
            {
                var newCustomer = new CustomerModel()
                {
                    Name = cus.Name,
                    Address = cus.Address,
                    PhoneNumber = cus.PhoneNumber
                };

                _myDBContext.CustomerModel.Add(newCustomer);

                _myDBContext.Entry(newCustomer).State = EntityState.Added;

                _myDBContext.SaveChanges();
                _myDBContext.Entry(newCustomer).State  = EntityState.Detached;

                res.Message = "Successfuly Added!";
                res.StatusCode = "200";

            }
            catch (Exception ex)
            {
                res.Error = ex.InnerException.Message;

            }
            

            return res;

        }

        [HttpDelete("DeleteCustomer")]
        public ResponseModel DeleteCustomer(int customerId)
        {

            var res = new ResponseModel();
            try
            {

                var customer = _myDBContext.CustomerModel.Find(customerId);

                if (customer == null)
                    throw new Exception("Customer not found!");

                _myDBContext.CustomerModel.Remove(customer);

                _myDBContext.Entry(customer).State = EntityState.Deleted;

                _myDBContext.SaveChanges();
                _myDBContext.Entry(customer).State = EntityState.Detached;


                res.Message = "Successfuly Deleted!";
                res.StatusCode = "200";

            }
            catch (Exception ex)
            {
                res.Error = ex.Message;

            }

            return res;
        }

        [HttpPost("UpdateCustomer")]
        public ResponseModel UpdateCustomer([FromBody] CustomerModel cus)
        {
            var res = new ResponseModel();
            try
            {
                var customer = _myDBContext.CustomerModel.Find(cus.Id);

                if (customer == null)
                    throw new Exception("Customer Not found");

                customer.Name = cus.Name;
                customer.Address = cus.Address;
                customer.PhoneNumber = cus.PhoneNumber;

                _myDBContext.CustomerModel.Update(customer);

                _myDBContext.Entry(customer).State = EntityState.Modified;

                _myDBContext.SaveChanges();
                _myDBContext.Entry(customer).State = EntityState.Detached;

                res.Message = "Successfuly Updated!";
                res.StatusCode = "200";

            }
            catch (Exception ex)
            {
                res.Error = ex.Message;

            }

            return res;
        }

    }
}
