How to use 


1. open solution using visual studio

2. change default connection string from application.json 

2. go to Package Manager Console and run 
     "Add-Migration DBInit"
     "Update-Migration"

3. press f5, it will load SWAGGER UI in https:localhost:4338/swagger 

4. run Angular 11
     -using cmd go to the path of Clientapp and run "ng serve"

5. access https:/localhost:4200/, it will load the Customer CRUD 