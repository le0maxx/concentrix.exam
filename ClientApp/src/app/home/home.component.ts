import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})


export class HomeComponent implements OnInit {

  displayedColumns: string[] = ['name', 'address', 'phoneNumber','action'];

  customerInfo = {
      id:0,
      name: '',
      address: '',
      phoneNumber: ''
  }




  customers : any[] = []

  constructor(private dataService: DataService) { }


  ngOnInit() {

    this.getCustomers()

  }

  getCustomers(){
    this.dataService.getRequest().subscribe((data: any) => {
      console.log(data)
      this.customers = data;

    })
  }


  addCustomer() {
    this.dataService.postRequest('addcustomer',this.customerInfo).subscribe((data:any) => {

      console.log(data)
      if(data.statusCode == "200"){
        this.customerInfo.name = ''
        this.customerInfo.address = ''
        this.customerInfo.phoneNumber = ''
        this.getCustomers()
      }

       

    })
    
  }

  editCustomer(customer:any){


    console.log(customer)
    this.customerInfo.id = customer.id;
    this.customerInfo.name = customer.name;
    this.customerInfo.address = customer.address;
    this.customerInfo.phoneNumber = customer.phoneNumber;


    
  }

  updateCustomer(){

    console.log(this.customerInfo)
      this.dataService.postRequest('updatecustomer',this.customerInfo).subscribe((data:any) => {

      console.log(data)
      if(data.statusCode == "200"){

        this.getCustomers()
      }
    })

  }

  deleteCustomer(id:number){

    this.dataService.deleteRequest('deletecustomer?customerId=' + id,).subscribe((data:any) => {

      if(data.statusCode == "200"){

        this.getCustomers()
      }

       

    })
  }

}
