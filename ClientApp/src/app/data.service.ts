import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private REST_API_SERVER = "https://localhost:44338/api/Customer/";

  

  constructor(private httpClient: HttpClient) { }

  public getRequest(){
    return this.httpClient.get(this.REST_API_SERVER);
  }

  public postRequest(apiType:string, param:object){
    
    const httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.httpClient.post(this.REST_API_SERVER + apiType, JSON.stringify(param),httpOptions)
  }

  public deleteRequest(apiType:string){
    
    const httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.httpClient.delete(this.REST_API_SERVER + apiType,)
  }
}
