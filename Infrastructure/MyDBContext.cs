﻿using concentrix.exam.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace concentrix.exam.Infrastructure
{
    public class MyDBContext : DbContext
    {
        public DbSet<CustomerModel> CustomerModel { get; set; }

        public MyDBContext(DbContextOptions<MyDBContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CustomerModel>().ToTable("Customer");

            modelBuilder.Entity<CustomerModel>().HasKey(x => x.Id).HasName("PK_Customer");

            modelBuilder.Entity<CustomerModel>().HasIndex(p => p.Name).IsUnique().HasDatabaseName("Idx_Name");

            modelBuilder.Entity<CustomerModel>().Property(ug => ug.Id).HasColumnType("int").UseMySqlIdentityColumn().IsRequired();
            modelBuilder.Entity<CustomerModel>().Property(ug => ug.Name).HasColumnType("nvarchar(100)").IsRequired();
            modelBuilder.Entity<CustomerModel>().Property(ug => ug.Address).HasColumnType("nvarchar(100)").IsRequired();
            modelBuilder.Entity<CustomerModel>().Property(ug => ug.PhoneNumber).HasColumnType("nvarchar(100)").IsRequired();

        }
    }
}
