﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace concentrix.exam.Models
{
    public class CustomerRequestModel
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
    }
}
