﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace concentrix.exam.Models
{
    public class ResponseModel
    {
        public string StatusCode { get; set; }
        public object Response { get; set; }
        public string Message { get; set; }
        public string Error { get; set; }
    }
}
